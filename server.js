const express = require('express');
const http = require('http');
const sockjs = require('sockjs');

const app = express();
const server = http.createServer(app);
const port = process.env.PORT || 10084;

// Tạo máy chủ SockJS
const sockjsServer = sockjs.createServer();
sockjsServer.installHandlers(server, { prefix: '/websocket' });

// Lưu trữ danh sách các kết nối
const connections = [];

// Xử lý kết nối SockJS
sockjsServer.on('connection', (conn) => {
    console.log('Client đã kết nối qua WebSocket');

    // Đăng ký client vào topic
    connections.push(conn);

    // Xử lý tin nhắn từ client
    conn.on('data', (message) => {
        // Gửi tin nhắn đến tất cả các 
        console.log("sss", message);
        connections.forEach((client) => {
            if (client !== conn) {
                client.write(message);
            }
        });
    });

    // Xử lý đóng kết nối
    conn.on('close', () => {
        console.log('Client đã đóng kết nối');
        const index = connections.indexOf(conn);
        if (index !== -1) {
            connections.splice(index, 1);
        }
    });
});

app.use(express.static(__dirname + '/public'));

app.get('/2', (req, res) => {
  res.sendFile(__dirname + '/public/index2.html');
});

app.get('/3', (req, res) => {
  res.sendFile(__dirname + '/public/index3.html');
});

server.listen(port, () => {
  console.log(`Máy chủ đang lắng nghe tại cổng ${port}`);
});




/* var express = require('express');
var http = require('http');
var sockjs = require('sockjs');
var StompServer = require('stompjs').Server;

var app = express();
var server = http.createServer(app);
var port = process.env.PORT || 3000;

var stompServer = sockjs.createServer();
var stompServerConnection = stompServer.installHandlers(server, { prefix: '/stomp' });

var messages = [];

stompServer.on('connection', function (conn) {
  console.log('Client đã kết nối qua STOMP');
  var stompServer = new StompServer({
    connect: function (headers, callback) {
      // Xác thực client (nếu cần)
      callback(null);
    },
    onUnhandledFrame: function (frame) {
      console.error('Khung không được xử lý:', frame);
    },
  });

  stompServer.on('subscribe', function (headers) {
    // Xử lý yêu cầu subscribe từ client
    // Ví dụ: Chúng ta sẽ đăng ký client vào topic '/topic/messages'
    stompServer.subscribe('/topic/messages', function (msg) {
      // Gửi tin nhắn đến client
      conn.write(JSON.stringify({
        destination: headers.destination,
        body: JSON.parse(msg.body),
      }));
    });
  });

  stompServer.listen(conn);
});

app.use(express.static(__dirname + '/public'));

server.listen(port, function () {
  console.log('Máy chủ đang lắng nghe tại cổng ' + port);
}); */
